package com.timofeev.mpapp;

import android.app.Application;
import android.content.Context;

import com.squareup.picasso.Picasso;
import com.timofeev.mpapp.utils.PicassoModule;

public class App extends Application {

    private static Context sContext;
    private static Picasso mPicasso;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
        mPicasso = PicassoModule.getPicasso(this);
    }

    public static Context getContext(){
        return sContext;
    }
    public static Picasso getPicasso(){
        return mPicasso;
    }}
