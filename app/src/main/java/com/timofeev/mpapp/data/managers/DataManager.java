package com.timofeev.mpapp.data.managers;

import com.timofeev.mpapp.App;
import com.timofeev.mpapp.data.network.ApiService;
import com.timofeev.mpapp.data.network.ResponseCallTransformer;
import com.timofeev.mpapp.data.network.ServiceGenerator;
import com.timofeev.mpapp.data.storage.dto.SongDto;

import rx.Observable;
import rx.schedulers.Schedulers;

public class DataManager {


    private PreferenceManagers mPreferenceManagers;
    private ApiService mApiService;


    private static DataManager INSTANCE = null;

    public static DataManager getInstance(){
        if(INSTANCE == null){
            INSTANCE = new DataManager();
        }
        return INSTANCE;
    }

    private DataManager() {
        mPreferenceManagers = new PreferenceManagers(App.getContext());
        mApiService = ServiceGenerator.createService();
    }


    //region ============= SongFragment ===============
    public void saveDesignState(int itemClick) {
        mPreferenceManagers.saveDesignCatalog(itemClick);
    }

    public int loadDesignState(){
        return mPreferenceManagers.loadDesignCatalog();
    }
    //endregion

    //region ============= Network ===============

    public Observable<SongDto> loadSongFromNetwork(String nameSinger){
        return mApiService.getListSong(nameSinger)
                .compose(new ResponseCallTransformer<>())
                .flatMap(songRes -> Observable.from(songRes.getResults()))
                .map(SongDto::new)
                .subscribeOn(Schedulers.io());

    }

    //endregion

}
