package com.timofeev.mpapp.data.storage.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.timofeev.mpapp.data.network.res.ResultsItem;

public class SongDto implements Parcelable {
    private final int id;
    private final String albumImgUrl;
    private final String artistName;
    private final String trackName;
    private final String trackPreviewUrl;

    public SongDto() {
        this.id = 1;
        this.albumImgUrl = "album";
        this.artistName = "artist";
        this.trackName = "track";
        this.trackPreviewUrl = "url";
    }

    public SongDto(ResultsItem resultsItem) {
        this.id = hashCode();
        this.albumImgUrl = resultsItem.getArtworkUrl100();
        this.artistName = resultsItem.getArtistName();
        this.trackName = resultsItem.getTrackName();
        this.trackPreviewUrl = resultsItem.getPreviewUrl();
    }

    protected SongDto(Parcel in) {
        id = in.readInt();
        albumImgUrl = in.readString();
        artistName = in.readString();
        trackName = in.readString();
        trackPreviewUrl = in.readString();
    }

    public static final Creator<SongDto> CREATOR = new Creator<SongDto>() {
        @Override
        public SongDto createFromParcel(Parcel in) {
            return new SongDto(in);
        }

        @Override
        public SongDto[] newArray(int size) {
            return new SongDto[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getAlbumImgUrl() {
        return albumImgUrl;
    }


    public String getArtistName() {
        return artistName;
    }


    public String getTrackName() {
        return trackName;
    }

    public String getTrackPreviewUrl() {
        return trackPreviewUrl;
    }

    @Override
    public String toString() {
        return super.toString()
                + " artistName: " + artistName
                + "\ntrackName: " + trackName
                + "\nurl: " + albumImgUrl
                + "\n trackUrl: " + trackPreviewUrl;
    }

    //region ============= Parcelable ===============

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(albumImgUrl);
        parcel.writeString(artistName);
        parcel.writeString(trackName);
        parcel.writeString(trackPreviewUrl);
    }

    //endregion

}
