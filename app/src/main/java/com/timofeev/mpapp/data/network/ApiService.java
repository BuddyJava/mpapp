package com.timofeev.mpapp.data.network;

import com.timofeev.mpapp.data.network.res.SongRes;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface ApiService {

    @GET("search")
    Observable<Response<SongRes>> getListSong(@Query("term") String search_word);

}
