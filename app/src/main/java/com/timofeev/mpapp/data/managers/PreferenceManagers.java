package com.timofeev.mpapp.data.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PreferenceManagers {

    private final SharedPreferences mSharedPreferences;
    private static final String DESIGN_CATALOG = "DESIGN_CATALOG";


    public PreferenceManagers(Context context) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void saveDesignCatalog(int state){
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(DESIGN_CATALOG, state);
        editor.apply();
    }

    public int loadDesignCatalog(){
        return mSharedPreferences.getInt(DESIGN_CATALOG, 0);
    }
}
