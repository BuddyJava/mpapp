package com.timofeev.mpapp.core;

public interface IView {

    void showProgress();
    void hideProgress();
}
