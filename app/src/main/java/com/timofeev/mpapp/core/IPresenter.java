package com.timofeev.mpapp.core;

import android.support.annotation.NonNull;

public interface IPresenter {

    void bindView(@NonNull IView view);
    void unbindView();
    IView getView();
}
