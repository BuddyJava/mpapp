package com.timofeev.mpapp.core;


import com.timofeev.mpapp.data.managers.DataManager;

public class BaseModel {

//    @Inject
    protected DataManager mDataManager;


    public BaseModel() {
//        DaggerModelComponent.create().inject(this);

        mDataManager = DataManager.getInstance();
    }
}
