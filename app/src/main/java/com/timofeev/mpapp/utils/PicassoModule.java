package com.timofeev.mpapp.utils;


import android.content.Context;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

public class PicassoModule {

    public static Picasso getPicasso(Context context){
        OkHttp3Downloader okHttp3Downloader = new OkHttp3Downloader(context);
        Picasso picasso = new Picasso.Builder(context)
            .downloader(okHttp3Downloader)
            .debugging(true)
            .build();
        Picasso.setSingletonInstance(picasso);
        return picasso;
    }
//
}
