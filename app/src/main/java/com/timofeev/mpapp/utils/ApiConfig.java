package com.timofeev.mpapp.utils;

public class ApiConfig {

    public static final String BASE_URL = "https://itunes.apple.com/";
    public static final int MAX_CONNECTION_TIMEOUT = 5000;
    public static final int MAX_READ_TIMEOUT = 5000;
    public static final int MAX_WRITE_TIMEOUT = 5000;
}
