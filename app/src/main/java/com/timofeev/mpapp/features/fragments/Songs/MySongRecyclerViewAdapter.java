package com.timofeev.mpapp.features.fragments.Songs;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.timofeev.mpapp.R;
import com.timofeev.mpapp.data.storage.dto.SongDto;

import java.util.ArrayList;
import java.util.List;


public class MySongRecyclerViewAdapter extends RecyclerView.Adapter<MySongRecyclerViewAdapter.ViewHolder> {

    private Picasso mPicasso;

    private List<SongDto> mValues = new ArrayList<>();
    private final SongFragment.OnListFragmentInteractionListener mListener;

    public MySongRecyclerViewAdapter(Picasso picasso, SongFragment.OnListFragmentInteractionListener listener) {
        mPicasso = picasso;
        mListener = listener;
    }

    public void addList(List<SongDto> values){
        mValues.clear();
        mValues.addAll(values);
        notifyDataSetChanged();
    }

    public MySongRecyclerViewAdapter(List<SongDto> items, SongFragment.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_song, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mSongNameTv.setText(mValues.get(position).getTrackName());
        holder.mSingerNameTv.setText(mValues.get(position).getArtistName());
        mPicasso.load(mValues.get(position)
                .getAlbumImgUrl())
                .into(holder.mAlbumView);

        holder.mView.setOnClickListener(v -> {
            if (null != mListener) {
                // Notify the active callbacks interface (the activity, if the
                // fragment is attached to one) that an item has been selected.
                mListener.onListFragmentInteraction(holder.mItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mAlbumView;
        public final TextView mSongNameTv;
        public final TextView mSingerNameTv;
        public SongDto mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mAlbumView = (ImageView) view.findViewById(R.id.album_img);
            mSongNameTv = (TextView) view.findViewById(R.id.song_name_tv);
            mSingerNameTv = (TextView) view.findViewById(R.id.singer_name_tv);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + mSingerNameTv.getText() + "'";
        }
    }
}
