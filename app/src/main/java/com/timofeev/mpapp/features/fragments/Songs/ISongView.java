package com.timofeev.mpapp.features.fragments.Songs;

import com.timofeev.mpapp.core.IView;


public interface ISongView extends IView {
    void initView(int countColumn);
    MySongRecyclerViewAdapter getAdapter();
    void showError(Throwable error);
}
