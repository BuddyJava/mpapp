package com.timofeev.mpapp.features.services.player;

import android.os.Binder;

public class MusicBinder extends Binder {

    private MusicService mMusicService;

    public MusicBinder(MusicService musicService) {

        mMusicService = musicService;
    }

    public MusicService getService(){
        return this.mMusicService;
    }
}
