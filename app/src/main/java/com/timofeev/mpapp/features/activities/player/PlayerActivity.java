package com.timofeev.mpapp.features.activities.player;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Parcelable;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.squareup.picasso.Picasso;
import com.timofeev.mpapp.App;
import com.timofeev.mpapp.R;
import com.timofeev.mpapp.core.BaseActivity;
import com.timofeev.mpapp.data.storage.dto.SongDto;
import com.timofeev.mpapp.features.services.player.MusicService;

public class PlayerActivity extends BaseActivity implements IPlayerView {

    private static final String CONTENT_EXTRA = "com.timofeev.content";
    private static final String CLOSE_EXTRA = "com.timofeev..mpapp.features.activities.close";
    private static final boolean CLOSE = true;

    private ServiceConnection mConnection;
    private ImageView mAlbumImageView;
    private CheckBox mPlayCheckBox;
    private SeekBar mProgress;

    private PlayerPresenter mPresenter = PlayerPresenter.getInstance();
    private Picasso mPicasso;


    //region ============= Instance ===============
    public static Intent closeInstance(Context context) {
        Intent intentActivity = new Intent(context, PlayerActivity.class);
        intentActivity.putExtra(CLOSE_EXTRA, CLOSE);
        return intentActivity;
    }

    public static Intent newInstance(Context context, Parcelable content) {
        Intent intentActivity = new Intent(context, PlayerActivity.class);
        intentActivity.putExtra(CONTENT_EXTRA, content);
        return intentActivity;
    }
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        mAlbumImageView = $(R.id.album_img);
        mPlayCheckBox = $(R.id.play_cbx);
        mProgress = $(R.id.progress);

        mPicasso = App.getPicasso();

        mPresenter.bindView(this);

    }


    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.initView(getIntent().getParcelableExtra(CONTENT_EXTRA),
                getIntent().getBooleanExtra(CLOSE_EXTRA, false));
        mPresenter.onStart();
        initCheckBox();
        initProgressBar();
    }

    private void initProgressBar() {
        mProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                mPresenter.changeProgress(seekBar.getProgress());
            }
        });
    }


    @Override
    protected void onDestroy() {
        mPresenter.onDestroy();
        super.onDestroy();
    }


    //region ============= Init ===============

    private void initCheckBox() {

        mPlayCheckBox.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                mPresenter.playMusic();
            } else {
                mPresenter.pauseMusic();
            }
        });
    }

    //endregion


    //region ============= IPlayerView ===============

    @Override
    public void initView(ServiceConnection musicConnection) {
        mConnection = musicConnection;
    }

    @Override
    public void bindCurService() {
        Intent intent = new Intent(this, MusicService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        startService(intent);
    }

    @Override
    public void unbindCurrentService() {
        unbindService(mConnection);
    }

    @Override
    public void setupAlbum(String url) {
        if (url != null) {
            mPicasso.load(url)
                    .into(mAlbumImageView);
        }
    }

    @Override
    public void statePlayButton(boolean state) {
        mPlayCheckBox.setChecked(state);
    }

    @Override
    public void setDuration(int duration) {
        mProgress.setMax(duration);
    }

    @Override
    public void updateProgress(int progress) {
        mProgress.setProgress(progress);
        mProgress.invalidate();
    }
    //endregion

}
