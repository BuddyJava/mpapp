package com.timofeev.mpapp.features.activities.catalog;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;

import com.timofeev.mpapp.R;
import com.timofeev.mpapp.core.BaseActivity;
import com.timofeev.mpapp.data.storage.dto.SongDto;
import com.timofeev.mpapp.features.activities.player.PlayerActivity;
import com.timofeev.mpapp.features.fragments.Songs.SongFragment;

public class CatalogActivity extends BaseActivity implements SongFragment.OnListFragmentInteractionListener {

    private Toolbar mToolbar;
//    private TextView mTextMessage;
//
//    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
//            = new BottomNavigationView.OnNavigationItemSelectedListener() {
//
//        @Override
//        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//            switch (item.getItemId()) {
//                case R.id.navigation_home:
//                    mTextMessage.setText(R.string.title_home);
//                    return true;
//                case R.id.navigation_dashboard:
//                    mTextMessage.setText(R.string.title_dashboard);
//                    return true;
//                case R.id.navigation_notifications:
//                    mTextMessage.setText(R.string.title_notifications);
//                    return true;
//            }
//            return false;
//        }
//
//    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalog);

        mToolbar = $(R.id.toolbar);

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.container);

        if (fragment == null) {
            fragmentManager.beginTransaction()
                    .replace(R.id.container, new SongFragment())
                    .commit();
        }

        initToolbar();

    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);

    }

    @Override
    public void onListFragmentInteraction(SongDto item) {
        startActivity(PlayerActivity.newInstance(this, item));
    }
}
