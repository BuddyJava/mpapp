package com.timofeev.mpapp.features.activities.player;

import android.content.ServiceConnection;

import com.timofeev.mpapp.core.IView;
import com.timofeev.mpapp.data.storage.dto.SongDto;

public interface IPlayerView extends IView{

    void initView(ServiceConnection connection);
    void bindCurService();
    void unbindCurrentService();
    void setupAlbum(String url);
    void statePlayButton(boolean state);
    void finish();

    void setDuration(int duration);
    void updateProgress(int progress);
}
