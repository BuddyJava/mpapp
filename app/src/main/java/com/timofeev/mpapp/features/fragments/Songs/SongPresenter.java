package com.timofeev.mpapp.features.fragments.Songs;

import android.content.res.Configuration;

import com.timofeev.mpapp.core.BasePresenter;
import com.timofeev.mpapp.data.storage.dto.SongDto;
import com.timofeev.mpapp.utils.L;

import java.util.ArrayList;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;

public class SongPresenter extends BasePresenter<ISongView> implements ISongPresenter {

    private final List<SongDto> mDtoList = new ArrayList<>();
    private static int mOrientation;
    private static final int COLUMN_LANSCAPE = 3;
    private static final int COLUMN_PORT = 2;
    public static final int GRID_LAYOUT = 0;
    public static final int LIST_LAYOUT = 1;

    private SongModel mModel = new SongModel();

    private static SongPresenter INSTANCE = null;

    public static SongPresenter getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new SongPresenter();
        }
        return INSTANCE;
    }

    private SongPresenter() {
    }

    @Override
    public void initPresenter(int configOrient) {
        mOrientation = configOrient;
        if (getView() != null) getView().getAdapter().addList(mDtoList);
        if (mModel.loadDesignState() == GRID_LAYOUT) {
            if (configOrient == Configuration.ORIENTATION_LANDSCAPE) {
                if (getView() != null) getView().initView(COLUMN_LANSCAPE);
            } else {
                if (getView() != null) getView().initView(COLUMN_PORT);
            }
        } else {
            if (getView() != null) getView().initView(LIST_LAYOUT);
        }

    }

    @Override
    public void clickOnChangeDesign(int itemClick) {
        mModel.saveDesignState(itemClick);
        if (getView() != null)
            initPresenter(mOrientation);

    }

    @Override
    public void clickOnSearch(String query) {
        L.e();
        if (getView() != null) getView().showProgress();
        mModel.getSongDtoObs(query)
                .observeOn(AndroidSchedulers.mainThread())
                .toList()
                .toSingle()
                .subscribe(songDtos -> {
                    if (getView() != null) {
                        mDtoList.clear();
                        mDtoList.addAll(songDtos);
                        getView().getAdapter().addList(mDtoList);
                        getView().hideProgress();
                    }
                }, throwable -> {
                    if (getView() != null) {
                        getView().showError(throwable);
                        getView().hideProgress();
                    }
                });
    }
}
