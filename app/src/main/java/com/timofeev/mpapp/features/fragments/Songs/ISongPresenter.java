package com.timofeev.mpapp.features.fragments.Songs;

import com.timofeev.mpapp.core.IPresenter;

public interface ISongPresenter extends IPresenter{

    void initPresenter(int configOrient);
    void clickOnChangeDesign(int itemClick);
    void clickOnSearch(String query);
}
