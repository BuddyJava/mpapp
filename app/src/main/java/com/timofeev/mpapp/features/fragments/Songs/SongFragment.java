package com.timofeev.mpapp.features.fragments.Songs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RadioGroup;

import com.jakewharton.rxbinding.support.v7.widget.RxSearchView;
import com.timofeev.mpapp.App;
import com.timofeev.mpapp.R;
import com.timofeev.mpapp.core.BaseFragment;
import com.timofeev.mpapp.data.storage.dto.SongDto;

import java.util.concurrent.TimeUnit;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class SongFragment extends BaseFragment implements ISongView, RadioGroup.OnCheckedChangeListener {

    private OnListFragmentInteractionListener mListener;
    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;
    private RadioGroup mRadioGroup;
    private Subscription mSearchSub;
    private MySongRecyclerViewAdapter mAdapter;
    private SongPresenter mSongPresenter = SongPresenter.getInstance();

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SongFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static SongFragment newInstance(int columnCount) {
        SongFragment fragment = new SongFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_song_list, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.list);
        mRadioGroup = (RadioGroup) view.findViewById(R.id.table);
        mRadioGroup.setOnCheckedChangeListener(this);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progress);
        mAdapter = new MySongRecyclerViewAdapter(App.getPicasso(), mListener);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        mSongPresenter.bindView(this);
        mSongPresenter.initPresenter(getResources().getConfiguration().orientation);
    }

    @Override
    public void onPause() {
        super.onPause();
        mSongPresenter.unbindView();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        if (mSearchSub != null) {
            mSearchSub.unsubscribe();
        }
    }


    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(SongDto item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_catalog, menu);
        initSearchView(menu);
    }


    //
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()){
//            case R.id.app_bar_clear:
//
//                break;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }
    //region ============= ISongView ===============

    @Override
    public void initView(int countColumn) {

        if (countColumn <= 1) {
            mRadioGroup.check(R.id.list_radio);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        } else {
            mRadioGroup.check(R.id.grid_radio);
            mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), countColumn));
        }
        if (mRecyclerView.getAdapter() == null) {
            mRecyclerView.setAdapter(mAdapter);
        }

    }

    @Override
    public MySongRecyclerViewAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void showError(Throwable error) {
        showSnackbar(error.getMessage());
    }

    //endregion

    //region ============= Events ===============

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
        switch (i) {
            case R.id.grid_radio:
                mSongPresenter.clickOnChangeDesign(SongPresenter.GRID_LAYOUT);
                break;
            case R.id.list_radio:
                mSongPresenter.clickOnChangeDesign(SongPresenter.LIST_LAYOUT);
                break;
        }
    }

    //endregion

    //region ============= Initiation ===============
    private SearchView initSearchView(Menu menu) {
        SearchView searchView = (SearchView) menu.findItem(R.id.app_bar_search).getActionView();

        mSearchSub = RxSearchView.queryTextChanges(searchView)
                .debounce(500, TimeUnit.MILLISECONDS)
                .filter(sequence -> sequence.length() >= 5)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(sequence -> mSongPresenter.clickOnSearch((String) sequence));
        return searchView;
    }
    //endregion
}
