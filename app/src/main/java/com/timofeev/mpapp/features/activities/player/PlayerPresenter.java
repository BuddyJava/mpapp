package com.timofeev.mpapp.features.activities.player;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Parcelable;

import com.timofeev.mpapp.core.BasePresenter;
import com.timofeev.mpapp.data.storage.dto.SongDto;
import com.timofeev.mpapp.features.services.player.MusicBinder;
import com.timofeev.mpapp.features.services.player.MusicService;
import com.timofeev.mpapp.utils.L;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

public class PlayerPresenter extends BasePresenter<IPlayerView> implements IPlayerPresenter {

    private SongDto mSongDto;
    private boolean mPlayState = true;
    private boolean mCloseState;
    private Subscription mProgressSub;
    private Subscription mDurationSub;
    private MusicService mMusicService;
    private ServiceConnection mMusicConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

            //get service
            if (mMusicService == null) {
                MusicBinder binder = (MusicBinder) service;
                mMusicService = binder.getService();
                mProgressSub = subscribeOnProgress();
            }

            //pass song
            if (mCloseState) {
                closeService();
            } else {
                mDurationSub = subscribeOnDuration();
                mMusicService.setSong(mSongDto);
                mMusicService.playMusic();
            }
//          musicBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
//            musicBound = false;
        }
    };

    private static PlayerPresenter INSTANCE = null;

    public static PlayerPresenter getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new PlayerPresenter();
        }
        return INSTANCE;
    }

    private PlayerPresenter() {

    }


    private void closeService() {
        if (mMusicService != null) {
            mMusicService.stopSelf();
            mMusicService = null;
            if (getView() != null) getView().finish();
        }
        if (mProgressSub != null) mProgressSub.unsubscribe();

        if (mDurationSub != null) mDurationSub.unsubscribe();
    }

    //region ============= Subscription ===============
    private Subscription subscribeOnProgress() {
        return mMusicService.getProgressObs()
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Integer>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        L.e(e);
                    }

                    @Override
                    public void onNext(Integer integer) {
                        L.e(integer);
                        if (getView() != null) getView().updateProgress(integer);
                    }
                });
    }

    private Subscription subscribeOnDuration() {
        return mMusicService.getDurationObs()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Integer>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        L.e();
                    }

                    @Override
                    public void onNext(Integer integer) {
                        L.e(integer);
                        if (getView() != null) getView().setDuration(integer);
                    }
                });
    }
    //endregion

    //region ============= IPlayerPresenter ===============

    @Override
    public void unbindView() {
        super.unbindView();
    }

    @Override
    public void initView(Parcelable songDto, boolean close) {

        mCloseState = close;

        if (songDto != null) {
            if (songDto instanceof SongDto) {
                mSongDto = ((SongDto) songDto);
            } else {
                throw new ClassCastException("need use " + SongDto.class);
            }
        }
        if (getView() != null) {
            getView().initView(mMusicConnection);
            getView().setupAlbum(mSongDto.getAlbumImgUrl());
            getView().statePlayButton(mPlayState);
        }

    }

    @Override
    public void onClickPlayOrPause(boolean closeState) {
        mPlayState = closeState;
        if (mPlayState) {
            mMusicService.playMusic();
        } else {
            mMusicService.pauseMusic();
        }
    }

    @Override
    public void onStart() {
        if (getView() != null) getView().bindCurService();
    }

    @Override
    public void onDestroy() {
        if (getView() != null) getView().unbindCurrentService();
    }

    @Override
    public void playMusic() {
        mMusicService.playMusic();
    }

    @Override
    public void pauseMusic() {
        mMusicService.pauseMusic();
    }

    @Override
    public void changeProgress(int progress) {
        mMusicService.setSeekTo(progress);
    }

    //endregion
}
