package com.timofeev.mpapp.features.services.player;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

public class MyServiceConnector implements ServiceConnection {
    private MusicService mMusicService;

    public MyServiceConnector(MusicService musicService) {
        this.mMusicService = musicService;
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {

    }
}
