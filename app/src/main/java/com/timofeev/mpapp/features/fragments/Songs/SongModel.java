package com.timofeev.mpapp.features.fragments.Songs;

import com.timofeev.mpapp.core.BaseModel;
import com.timofeev.mpapp.data.storage.dto.SongDto;

import rx.Observable;

public class SongModel extends BaseModel {

    public void saveDesignState(int itemClick) {
        mDataManager.saveDesignState(itemClick);
    }

    public int loadDesignState(){
        return mDataManager.loadDesignState();
    }

    public Observable<SongDto> getSongDtoObs(String singerName){
        return mDataManager.loadSongFromNetwork(singerName);
    }
}
