package com.timofeev.mpapp.features.activities.player;

import android.os.Parcelable;

import com.timofeev.mpapp.core.IPresenter;
import com.timofeev.mpapp.data.storage.dto.SongDto;

public interface IPlayerPresenter extends IPresenter {
    void initView(Parcelable songDto, boolean close);
    void onClickPlayOrPause(boolean state);
    void onStart();
    void onDestroy();

    void playMusic();

    void pauseMusic();

    void changeProgress(int progress);
}
