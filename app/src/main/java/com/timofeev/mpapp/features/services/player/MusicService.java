package com.timofeev.mpapp.features.services.player;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.PlaybackParams;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.NotificationCompat;

import com.timofeev.mpapp.R;
import com.timofeev.mpapp.data.storage.dto.SongDto;
import com.timofeev.mpapp.features.activities.player.PlayerActivity;
import com.timofeev.mpapp.utils.L;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Single;
import rx.schedulers.Schedulers;

public class MusicService extends Service implements MediaPlayer.OnPreparedListener,
        MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {

    private static final int NOTIFY_ID = 1;
    private MediaPlayer mMediaPlayer;
    private SongDto mSongDto;
    private MusicBinder mMusicBinder = new MusicBinder(this);
    private boolean newSong = true;
    private boolean playerPause;


    public MusicService() {
        mSongDto = new SongDto();
    }

    @Override
    public IBinder onBind(Intent intent) {
        L.e();
        return mMusicBinder;
    }

    //region ============= LifeCycle ===============

    @Override
    public void onCreate() {
        super.onCreate();
        L.e();
        mMediaPlayer = new MediaPlayer();
        initMediaPlayer();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        L.e();
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        L.e();
        super.onDestroy();
        if (mMediaPlayer != null) mMediaPlayer.release();
    }

    //endregion


    private void initMediaPlayer() {

        mMediaPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        mMediaPlayer.setOnCompletionListener(this);
        mMediaPlayer.setOnErrorListener(this);
        mMediaPlayer.setOnPreparedListener(this);
    }

    public void playMusic() {
        playerPause = false;

        if (newSong) {
            mMediaPlayer.reset();
            try {

                mMediaPlayer.setDataSource(mSongDto.getTrackPreviewUrl());
                mMediaPlayer.prepareAsync();
                createNotify(mSongDto);
                newSong = false;
            } catch (IOException e) {
                e.printStackTrace();
                L.e(e);
            }

        } else {
            mMediaPlayer.start();
        }
//        mMediaPlayer.release();

    }

    //region ============= Events ===============

    @Override
    public void onCompletion(MediaPlayer mp) {
        L.e();
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        L.e();
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        L.e();
        if (!playerPause) {
            mp.start();
        }

//        createNotify(mSongDto);
    }

    private void createNotify(SongDto songDto) {

        Intent notIntent = new Intent(this, PlayerActivity.class);
        notIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendInt = PendingIntent.getActivity(this, 0,
                notIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        PendingIntent closePendInt = PendingIntent.getActivity(this, 1,
                PlayerActivity.closeInstance(this), PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());

        builder.setContentIntent(pendInt)
                .setSmallIcon(R.drawable.ic_play_arrow_white_24dp)
                .setTicker(songDto.getTrackName())
                .setOngoing(true)
                .setContentTitle(songDto.getArtistName())
                .addAction(R.drawable.ic_close_white_24dp, "Закрыть", closePendInt)
                .setContentText(songDto.getTrackName());

        startForeground(NOTIFY_ID, builder.build());
    }

    public void setSong(SongDto songDto) {
        if (songDto.getId() != mSongDto.getId()) {
            newSong = true;
            mSongDto = songDto;
        }

    }

    public void pauseMusic() {
        playerPause = true;
        if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {

            mMediaPlayer.pause();
        }
    }

    private int getProgress() {
        return mMediaPlayer.getCurrentPosition() / 1000;
    }

    public Observable<Integer> getProgressObs() {
        return Observable.interval(1, TimeUnit.SECONDS)
                .map(aLong -> getProgress())
                .subscribeOn(Schedulers.io());
    }

    public Single<Integer> getDurationObs() {
        return Single.just(29)
                .subscribeOn(Schedulers.io());
    }

    public void setSeekTo(int progress) {
        int goal = progress * 1000;
        L.e(goal);
        mMediaPlayer.seekTo(goal);
    }

    //endregion

}
